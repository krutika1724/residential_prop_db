import pymysql

from flask import Flask, jsonify, request
from flask_cors import CORS
import pymysql

app = Flask(__name__)
cors = CORS(app)

@app.route('/property', methods=['GET'])
def get_users():
    # To connect MySQL database
    conn = pymysql.connect(host='localhost', user='phpuser', password = "%TGBbgt5", db='residentialprop_db')

    cur = conn.cursor()
    cur.execute("select * from residential_prop LIMIT 20")
    output = cur.fetchall()

    print(type(output)); #this will print tuple

    for rec in output:
        print(rec);

    # To close the connection
    conn.close()

    return jsonify(output);

@app.route('/property/id', methods=['GET'])
def read():
    # To connect MySQL database
    conn = pymysql.connect(host='localhost', user='phpuser', password = "%TGBbgt5", db='residentialprop_db')
    cur = conn.cursor()
    id = int(request.args.get('id'))
    cur.execute(f"select * from residential_prop WHERE id = {id}");

    output = cur.fetchall()

    print(type(output)); #this will print tuple

    for rec in output:
        print(rec);

    # To close the connection
    conn.close()

    return jsonify(output);


@app.route('/property', methods=['DELETE'])
def deleteRecord():
    conn = pymysql.connect(host='localhost', user='phpuser', password = "%TGBbgt5", db='residentialprop_db')
    cur = conn.cursor()
    id = int(request.args.get('id'));

    query = f"delete from residential_prop where id = {id}";
    #print(query)
    res = cur.execute(query);
    conn.commit();
    print(cur.rowcount, "record(s) deleted")

    return "Record deleted sussesfully"

@app.route('/property', methods=['POST'])
def insertRecord():
    conn= pymysql.connect(host='localhost',user='phpuser',password= "%TGBbgt5",db='residentialprop_db')

    #get raw json values
    raw_json = request.get_json();
    RealEstate = raw_json['RealEstate'];
    FirstName = raw_json['FirstName'];
    address = raw_json['address'];
    T_amount = raw_json['T_amount'];


    sql = f"INSERT INTO residential_prop (ID,RealEstate,FirstName,address,T_amount,R_date) VALUES (NULL,'{RealEstate}','{FirstName}','{address}','{T_amount}',NOW())";
    cur= conn.cursor()

    cur.execute(sql);
    conn.commit()
    return "Record inserted Succesfully"

@app.route('/property', methods=['PUT'])
def updateRecord():
    conn= pymysql.connect(host='localhost',user='phpuser',password= "%TGBbgt5",db='residentialprop_db')

    raw_json = request.get_json();

    #print(type(raw_json));

    raw_json = request.get_json();
    ID = raw_json['ID'];
    RealEstate = raw_json['RealEstate'];
    FirstName = raw_json['FirstName'];
    address = raw_json['address'];
    sql_update_quary=(f"UPDATE residentialprop_db.residential_prop SET RealEstate = '{RealEstate}',FirstName = '{FirstName}',address = '{address}' WHERE id = '{ID}'");
    cur= conn.cursor()
    cur.execute(sql_update_quary);
    conn.commit()
    return "Record Updated Sussecfully";


if __name__ == "__main__":
    app.run(debug=True);
